﻿using UnityEngine;
using System.Collections.Generic;

public class CameraFollowerBehaviour : MonoBehaviour {

	private static CameraFollowerBehaviour instance;
	public static CameraFollowerBehaviour get {
		get {
			return instance;
		}
	}

	void Awake() {
		instance = this;
	}

	List<Transform> transformsToFollow = new List<Transform>();

	public void AddTransformToFollow( Transform transform ) {
		transformsToFollow.Add( transform );
	}

	public void RemoveTransformToFollow( Transform transform ) {
		transformsToFollow.Remove( transform );
	}

	void FixedUpdate() {
		if ( transformsToFollow.Count > 0 ) {
			Vector3 averagePosition = Vector3.zero;
			foreach ( Transform t in transformsToFollow ) {
				averagePosition += t.position;
			}
			averagePosition /= transformsToFollow.Count;

			Vector3 newSmoothedPosition = ( transform.position + averagePosition ) / 2;
			newSmoothedPosition.z = -10;
			transform.position = newSmoothedPosition;
		}
	}

}
