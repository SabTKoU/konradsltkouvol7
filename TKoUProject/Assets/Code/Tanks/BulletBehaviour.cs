﻿using UnityEngine;
using System.Collections;

public class BulletBehaviour : MonoBehaviour {

	TankBehaviour owner;

	public void GoForward( TankBehaviour owner, float velocity, float distance) {
		this.owner = owner;
		Rigidbody2D body = GetComponent<Rigidbody2D>();
		body.velocity = transform.right * velocity;
		StartCoroutine( DestroyAndSpawnTankAfterDelay( distance / velocity ) );
	}

	IEnumerator DestroyAndSpawnTankAfterDelay( float delay ) {
		yield return new WaitForSeconds( delay );
		GameObject.Destroy( gameObject );
		owner.GetController().TryToSpawnNewTank( transform.position );
	}

	void DestroyNow() {
		StopAllCoroutines();
		GameObject.Destroy( gameObject );
	}

	void OnCollisionEnter2D( Collision2D collision ) {
		if ( collision.collider.tag == "Tank" ) {
			TankBehaviour tank = collision.collider.GetComponent<TankBehaviour>();
			if ( ( tank != null ) && ( tank != owner ) ) {
				tank.GotShot();
				DestroyNow();
			}
		}
	}
}
