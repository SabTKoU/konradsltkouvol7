﻿using UnityEngine;
using System.Collections.Generic;

public class TanksControllerBehaviour : MonoBehaviour {

	[System.Serializable]
	public class TankAndBulletSettings {
		public float rotateInterval = .5f;
		public float tankRotationFrom = 15;
		public float tankRotationTo = 45;
		public float tankActiveAfter = 6;
		public float bulletDistanceFrom = 1;
		public float bulletDistanceTo = 4;
		public float bulletVelocity = 4;
		public GameObject bulletPrefab;
		public uint  shootsToMakePerTank = 12;
	}
	
	public GameObject tankPrefab;
	public uint maxTanksAliveToStartCleanUp = 100;
	public TankAndBulletSettings tankAndBulletSettings = new TankAndBulletSettings();

	List<TankBehaviour> spawnedTanks = new List<TankBehaviour>();
	bool stopSpawn = false;

	void Start() {
		TryToSpawnNewTank( Vector2.zero, false );
	}

	void FixedUpdate() {
		if ( !stopSpawn && ( spawnedTanks.Count >= maxTanksAliveToStartCleanUp ) ) {
			stopSpawn = true;
			foreach ( TankBehaviour tankBeh in spawnedTanks ) {
				tankBeh.SetState( TankBehaviour.State.ACTIVE );
				tankBeh.SetRemainingShoots( tankAndBulletSettings.shootsToMakePerTank );
			}
		}
	}

	public void TryToSpawnNewTank( Vector2 position, bool activationDelayAfterSpawn = true ) {
		if ( !stopSpawn ) {
			GameObject spawned = GameObject.Instantiate( tankPrefab, position, new Quaternion() ) as GameObject;
			TankBehaviour tankBeh = spawned.GetComponent<TankBehaviour>();

			tankBeh.SetController( this );
			tankBeh.SetRemainingShoots( tankAndBulletSettings.shootsToMakePerTank );
			if ( activationDelayAfterSpawn ) {
				tankBeh.SetActiveAfter( tankAndBulletSettings.tankActiveAfter );
			} else {
				tankBeh.SetState( TankBehaviour.State.ACTIVE );
			}

			spawnedTanks.Add( tankBeh );
		}
	}

	public void RemoveTank( TankBehaviour tankBeh ) {
		spawnedTanks.Remove( tankBeh );
	}

	
	const float GUI_WIDTH = 300;
	const float GUI_HEIGHT = 100;

	void OnGUI() {
		Rect r = new Rect( 0, 0, GUI_WIDTH, GUI_HEIGHT );
		GUI.Label( r, "Towers: " + spawnedTanks.Count );
	}
}
