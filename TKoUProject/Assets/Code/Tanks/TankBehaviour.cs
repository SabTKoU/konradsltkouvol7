﻿using UnityEngine;
using System.Collections;

public class TankBehaviour : MonoBehaviour {

	public enum State {
		ACTIVE, INACTIVE
	}
	State state = State.INACTIVE;

	public void SetState( State s ) {
		state = s;
		RefreshSpriteRenderersColor();
	}

	void RefreshSpriteRenderersColor() {
		if ( state == State.ACTIVE ) {
			foreach ( SpriteRenderer r in GetComponentsInChildren<SpriteRenderer>() ) {
				r.color = Color.red;
			}
			
		} else {
			foreach ( SpriteRenderer r in GetComponentsInChildren<SpriteRenderer>() ) {
				r.color = Color.white;
			}
		}
	}
	
	public Transform spawnBulletsFrom;

	uint remainingShoots;
	float setActiveTime;
	TanksControllerBehaviour.TankAndBulletSettings settings;
	TanksControllerBehaviour controller;

	public TanksControllerBehaviour GetController() {
		return controller;
	}

	public void SetController( TanksControllerBehaviour controller ) {
		this.controller = controller;
		settings = controller.tankAndBulletSettings;
	}

	public void SetActiveAfter( float delay ) {
		StartCoroutine( ActivateAfter( settings.tankActiveAfter ) );
	}

	IEnumerator ActivateAfter( float delay ) {
		yield return new WaitForSeconds( delay );
		SetState( State.ACTIVE );
	}
	
	public void SetRemainingShoots( uint shoots ) {
		remainingShoots = shoots;
	}
	
	public void GotShot() {
		StopAllCoroutines();
		controller.RemoveTank( this );
		GameObject.Destroy( this.gameObject );
	}

	void Start() {
		CameraFollowerBehaviour.get.AddTransformToFollow( transform );
	}

	void OnDestroy() {
		CameraFollowerBehaviour.get.RemoveTransformToFollow( transform );
	}

	float lastRotationTime;

	void Update() {
		if ( state == State.ACTIVE ) {
			if ( remainingShoots > 0 ) {
				if ( Time.time > lastRotationTime + settings.rotateInterval ) {
					lastRotationTime = Time.time;
					Rotate();
					Shoot();

					remainingShoots--;
				}

			} else {
				SetState( State.INACTIVE );
			}
		}
	}

	void Rotate() {
		transform.Rotate( 0, 0, Random.Range( settings.tankRotationFrom, settings.tankRotationTo ) );
	}

	void Shoot() {
		GameObject spawned = GameObject.Instantiate( settings.bulletPrefab, spawnBulletsFrom.position, transform.rotation ) as GameObject;
		BulletBehaviour bulletBeh = spawned.GetComponent<BulletBehaviour>();
		bulletBeh.GoForward( this, settings.bulletVelocity, Random.Range( settings.bulletDistanceFrom, settings.bulletDistanceTo ) );
	}

}
