﻿using UnityEngine;
using System.Collections.Generic;

public class BallsController : MonoBehaviour {

	[System.Serializable]
	public class BallSettings {
		public float scaleAdditionPerEat = 0.15f;
		public float massAdditionPerEat = 0.1f;
		public uint eatsToExplode = 50;
		public float explodedColliderDisableDuration = 0.5f;
		public float explodePowerFrom = 5;
		public float explodePowerTo = 10;
	}
	public BallSettings ballSettings;

	public GameObject ballPrefab;
	[Header( "Sprite which indicates, where the balls can be spawned." )]
	public Renderer backgroundSpaceRenderer;

	public uint ballsCountToStopEatingAndSpawning = 250;
	public float spawnInterval = 0.25f;

	[HideInInspector]
	public bool eatingAndSpawningAllowed = true;

	float lastSpawnTime;

	List<BallBehaviour> spawnedBalls = new List<BallBehaviour>();
	int everSpawnedCounter = 0;

	void Update() {
		if ( eatingAndSpawningAllowed ) {
			if ( Time.time > lastSpawnTime + spawnInterval ) {
				SpawnGameplayBall();
				lastSpawnTime = Time.time;
			}

			if ( everSpawnedCounter >= ballsCountToStopEatingAndSpawning ) {
				eatingAndSpawningAllowed = false;
				foreach ( BallBehaviour ball in spawnedBalls ) {
					ball.ReverseEffector();
				}
			}
		}
	}

	void SpawnGameplayBall() {
		Bounds b = backgroundSpaceRenderer.bounds;
		float x = Random.Range( b.min.x, b.max.x );
		float y = Random.Range( b.min.y, b.max.y );
		BallBehaviour beh = SpawnBallAtPosition( new Vector2( x, y ) );
		spawnedBalls.Add( beh );
		everSpawnedCounter++;
	}

	BallBehaviour SpawnBallAtPosition( Vector3 position ) {
		GameObject spawned = GameObject.Instantiate( ballPrefab, position, new Quaternion() ) as GameObject;
		BallBehaviour ballBeh = spawned.GetComponent<BallBehaviour>();
		ballBeh.controller = this;
		return ballBeh;
	}

	public void SpawnExplodedBall( Vector3 position ) {
		BallBehaviour b = SpawnBallAtPosition( position );
		b.ApplyRandomForce();
		b.TemporarlyDeactivateColliders();
		spawnedBalls.Add( b );
	}

	public void RemoveBall( BallBehaviour ball ) {
		spawnedBalls.Remove( ball );
	}
		
	const float GUI_WIDTH = 300;
	const float GUI_HEIGHT = 100;
	
	void OnGUI() {
		Rect r = new Rect( 0, 0, GUI_WIDTH, GUI_HEIGHT );
		GUI.Label( r, "Balls spawned: " + everSpawnedCounter );
	}

}
