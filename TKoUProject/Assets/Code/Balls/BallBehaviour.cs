﻿using UnityEngine;
using System.Collections;

public class BallBehaviour : MonoBehaviour {

	[HideInInspector]
	public bool eaten = false;

	[HideInInspector]
	public BallsController controller;

	PointEffector2D pointEffector;
	uint eatenBalls = 0;

	public uint GetEatenBalls() {
		return eatenBalls;
	}

	void Start() {
		Time.timeScale = 10;
		pointEffector = GetComponentInChildren<PointEffector2D>();
	}

	void OnCollisionEnter2D( Collision2D collision ) {
		if ( !eaten ) {
			if ( controller != null ) { // in case collision happens while spawning, before assigning controller
				if ( controller.eatingAndSpawningAllowed ) {
					BallBehaviour otherBall = collision.collider.GetComponent<BallBehaviour>();
					if ( eatenBalls >= otherBall.GetEatenBalls() ) {
						Eat( otherBall );
					}
				}
			}
		}
	}

	void Eat( BallBehaviour otherBall ) {
		otherBall.eaten = true;
		controller.RemoveBall( otherBall );
		GameObject.Destroy( otherBall.gameObject );

		uint howMuchToGrow = (uint)Mathf.Max( 1, otherBall.GetEatenBalls() );
		eatenBalls += howMuchToGrow;

		if ( eatenBalls >= controller.ballSettings.eatsToExplode ) {
			Explode();
		} else {
			transform.localScale = transform.localScale + ( Vector3.one * controller.ballSettings.scaleAdditionPerEat * howMuchToGrow );
			GetComponent<Rigidbody2D>().mass += controller.ballSettings.massAdditionPerEat;
		}
	}

	void Explode() {
		controller.RemoveBall( this );
		for ( int i = 0; i < eatenBalls; i++ ) {
			controller.SpawnExplodedBall( transform.position );
		}
		GameObject.Destroy( gameObject );
	}

	public void ReverseEffector() {
		if ( pointEffector != null ) {
			pointEffector.forceMagnitude = -pointEffector.forceMagnitude;
		}
	}

	public void ApplyRandomForce() {
		Vector3 force = 
			Random.insideUnitCircle * Random.Range( controller.ballSettings.explodePowerFrom, controller.ballSettings.explodePowerTo );
		GetComponent<Rigidbody2D>().AddForce( force, ForceMode2D.Impulse );
	}

	public void TemporarlyDeactivateColliders() {
		foreach ( Collider2D collider in GetComponentsInChildren<Collider2D>() ) {
			collider.enabled = false;
		}
		Invoke( "EnableColliders", controller.ballSettings.explodedColliderDisableDuration );
	}

	void EnableColliders() {
		foreach ( Collider2D collider in GetComponentsInChildren<Collider2D>() ) {
			collider.enabled = true;
		}
	}
}
